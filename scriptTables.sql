CREATE TABLE Paciente (
    DNI VARCHAR(64) PRIMARY KEY,
    nombre VARCHAR(64),
    telefono INTEGER,
    mail VARCHAR(64)
);

CREATE TABLE ObraSocial (
    nroAfiliado VARCHAR(64) PRIMARY KEY,
    afiliadoDNI VARCHAR(64) REFERENCES Paciente(DNI),
    empresaNombre VARCHAR(64),
    fechaEmision DATE,
    fechaCaducidad DATE
);

CREATE TABLE EquipoMedico (
    idEquipo VARCHAR(64) PRIMARY KEY,
    nombre VARCHAR(64),
    descripcion VARCHAR(255)
);

CREATE TABLE ServicioMedico (
    idServicio VARCHAR(64) PRIMARY KEY,
    nombreServicio VARCHAR(64),
    idEquipo VARCHAR(64) REFERENCES EquipoMedico(idEquipo)
);

CREATE TABLE Turno (
    nroTramite INTEGER PRIMARY KEY,
    pacienteAfiliado VARCHAR(64) REFERENCES ObraSocial(nroAfiliado),
    idServicio VARCHAR(64) REFERENCES ServicioMedico(idServicio),
    fechaAtencion DATE,
    estado VARCHAR(64)
);

CREATE TABLE Medico (
    idMedico VARCHAR(64) PRIMARY KEY,
    nombre VARCHAR(64),
    idServicio VARCHAR(64) REFERENCES ServicioMedico(idServicio) 
);

CREATE TABLE TurnoDeTrabajo (
    idMedico VARCHAR(64) REFERENCES Medico (idMedico),
    diaSemana VARCHAR(10),
    horaInicio TIME,
    horaFin TIME,
    PRIMARY KEY (idMedico, diaSemana, horaInicio, horaFin)
);
/* Tablas Con PK y FK creadas */