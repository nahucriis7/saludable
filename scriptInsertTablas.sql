INSERT INTO Paciente (DNI, nombre, telefono, mail)
VALUES ('30678912', 'Juan Perez', 1122334455, 'juan.perez@mail.com'),
       ('23987654', 'Maria Rodriguez', 1122334466, 'maria.rodriguez@mail.com'),
       ('40876543', 'Carlos Gonzalez', 1122334477, 'carlos.gonzalez@mail.com'),
       ('38901234', 'Ana Fernandez', 1122334488, 'ana.fernandez@mail.com'),
       ('27890123', 'Pedro Diaz', 1122334499, 'pedro.diaz@mail.com'),
       ('34678901', 'Laura Gomez', 1122334400, 'laura.gomez@mail.com'),
       ('42789012', 'Diego Martinez', 1122334411, 'diego.martinez@mail.com'),
       ('19234567', 'Silvia Alvarez', 1122334422, 'silvia.alvarez@mail.com'),
       ('27568901', 'Jorge Lopez', 1122334433, 'jorge.lopez@mail.com'),
       ('17543298', 'Marta Sanchez', 1122334444, 'marta.sanchez@mail.com');

/************************************************************************/

INSERT INTO ObraSocial (nroAfiliado, afiliadoDNI, empresaNombre, fechaEmision, fechaCaducidad)
VALUES ('11111111', '30678912', 'OSDE',   '2022-02-15', '2025-02-15'),
       ('22222222', '30678912', 'Galeno', '2022-06-26', '2025-06-26'),
       ('33333333', '23987654', 'IOMA',   '2022-10-28', '2025-10-28'),
       ('44444444', '40876543', 'OSDE',   '2022-03-22', '2025-03-22'),
       ('55555555', '38901234', 'OSECAC', '2022-07-01', '2025-07-01'),
       ('66666666', '27890123', 'OSECAC', '2022-11-05', '2025-11-05'),
       ('77777777', '34678901', 'IOMA',   '2022-01-18', '2025-01-18'),
       ('88888888', '42789012', 'IOMA',   '2022-05-14', '2025-05-14'),
       ('99999999', '19234567', 'Galeno', '2022-08-31', '2025-08-31'),
       ('10101010', '27568901', 'Galeno', '2022-12-20', '2025-12-20');

/************************************************************************/
INSERT INTO EquipoMedico (idEquipo, nombre, descripcion)
VALUES ('S8T4K0L1', 'Equipo 1', 'Radioscopio, intensificador de imágenes, dosímetro'),
       ('J5M1N6D9', 'Equipo 2', 'Tomógrafo computarizado, simulador de tratamiento, dosímetro'),
       ('R3X9Y7Z1', 'Equipo 3', 'Ecógrafo, transductor, analizador de imágenes'),
       ('G2H8F4E1', 'Equipo 4', 'Tijeras quirúrgicas, instrumental de sutura, jeringas y agujas');


/************************************************************************/
INSERT INTO ServicioMedico (idServicio, nombreServicio, idEquipo)
VALUES ('23G5K6H8', 'Radiologia', 'S8T4K0L1'),
       ('12Y8R7T9', 'Tomografia', 'J5M1N6D9'),
       ('25A6F3Z1', 'Ecografia', 'R3X9Y7Z1'),
       ('18B9N2J7', 'Traumatologia y Ortopedia', 'G2H8F4E1'),
       ('20L3P6Q9', 'Obstetricia y Ginecologia', 'G2H8F4E1');


/************************************************************************/
INSERT INTO Turno (nroTramite, pacienteAfiliado, idServicio, fechaAtencion, estado) 
VALUES (12345678, '11111111', '23G5K6H8', '2023-03-30', 'Completada'),
       (23456789, '22222222', '12Y8R7T9', '2023-04-02', 'Aceptada'),
       (34567890, '33333333', '25A6F3Z1', '2023-04-04', 'Aceptada'),
       (45678901, '44444444', '18B9N2J7', '2023-03-25', 'Completada'),
       (56789012, '55555555', '20L3P6Q9', '2023-04-03', 'Aceptada'),
       (67890123, '66666666', '23G5K6H8', '2023-04-06', 'Aceptada'),
       (78901234, '77777777', '12Y8R7T9', '2023-03-28', 'Completada'),
       (89012345, '88888888', '25A6F3Z1', '2023-03-29', 'Completada'),
       (90123456, '99999999', '18B9N2J7', '2023-04-01', 'Aceptada'),
       (12345679, '10101010', '20L3P6Q9', '2023-04-05', 'Aceptada'),
       (23456780, '11111111', '23G5K6H8', '2023-04-07', 'Cancelada'),
       (34567891, '22222222', '12Y8R7T9', '2023-04-08', 'Aceptada'),
       (45678902, '33333333', '25A6F3Z1', '2023-04-10', 'Pendiente'),
       (56789013, '44444444', '18B9N2J7', '2023-04-12', 'Pendiente'),
       (67890124, '55555555', '20L3P6Q9', '2023-04-14', 'Pendiente'),
       (78901235, '66666666', '23G5K6H8', '2023-04-16', 'Pendiente'),
       (89012346, '77777777', '12Y8R7T9', '2023-03-18', 'Expirada'),
       (90123457, '88888888', '25A6F3Z1', '2023-04-20', 'Modificación Pendiente'),
       (12345670, '99999999', '18B9N2J7', '2023-04-22', 'Pendiente'),
       (23456781, '10101010', '20L3P6Q9', '2023-04-24', 'Pendiente');


/************************************************************************/
INSERT INTO Medico (idMedico, nombre, idServicio)
VALUES ('23567899qwer1990', 'Dr. Juan Gomez', '23G5K6H8'),
       ('29876543tyui2001', 'Dra. Maria Castillo', '23G5K6H8'),
       ('25678901asdf1986', 'Dr. Carlos Abal', '12Y8R7T9'),
       ('32890123ghjk2010', 'Dra. Ana Molina', '25A6F3Z1'),
       ('30987654zxcv1995', 'Dr. Pedro Gutierrez', '18B9N2J7'),
       ('31567890bnml1984', 'Dra. Laura Guerreyro', '20L3P6Q9'),
       ('29543210poui1988', 'Dr. Jorge Morales', '20L3P6Q9');


/************************************************************************/
INSERT INTO TurnoDeTrabajo (idMedico, diaSemana, horaInicio, horaFin)
VALUES ('23567899qwer1990', 'Lunes', '08:00:00', '14:00:00'),
       ('29876543tyui2001', 'Lunes', '14:00:00', '20:00:00'),
       ('25678901asdf1986', 'Lunes', '08:00:00', '14:00:00'),
       ('32890123ghjk2010', 'Lunes', '14:00:00', '20:00:00'),
       ('30987654zxcv1995', 'Martes', '08:00:00', '14:00:00'),
       ('31567890bnml1984', 'Martes', '14:00:00', '20:00:00'),
       ('29543210poui1988', 'Martes', '08:00:00', '14:00:00'),
       ('29876543tyui2001', 'Martes', '14:00:00', '20:00:00'),
       ('30987654zxcv1995', 'Miercoles', '08:00:00', '14:00:00'),
       ('25678901asdf1986', 'Miercoles', '14:00:00', '20:00:00'),
       ('31567890bnml1984', 'Jueves', '08:00:00', '14:00:00'),
       ('29876543tyui2001', 'Jueves', '14:00:00', '20:00:00'),
       ('29543210poui1988', 'Viernes', '08:00:00', '14:00:00'),
       ('32890123ghjk2010', 'Viernes', '14:00:00', '20:00:00'),
       ('23567899qwer1990', 'Sabado', '08:00:00', '10:00:00'),
       ('30987654zxcv1995', 'Sabado', '10:00:00', '12:00:00'),
       ('23567899qwer1990', 'Domingo', '08:00:00', '14:00:00'),
       ('25678901asdf1986', 'Domingo', '10:00:00', '16:00:00'),
       ('29543210poui1988', 'Domingo', '14:00:00', '20:00:00');